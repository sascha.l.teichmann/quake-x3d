# Quake X3D

This are some level data sets in [X3D](https://www.web3d.org/) format from
the shareware version of [Quake I](https://archive.org/details/Quake_802).
The data is extracted from a Quake I installation,
converted to VRML with [bsp2wrl](https://www.quaddicted.com/files/idgames2/utils/level_edit/converters/),
and converted to X3D with [view3dscene](https://castle-engine.io/view3dscene.php)

This is for demo purposes only. Do not use it otherwise!

You can buy a valid licenced copy of Quake at [GOG](https://www.gog.com/en/game/quake_the_offering).
